Port of Moitah's CenterCut 1.4.0 WinAmp plugin to LADSPA, by Patrice Levesque.
Original covered under GPL v2-or-later, as is this port.  Many thanks to the
original author.

This plugin keeps the center "channel" of stereo audio and discards the rest
(opposite of OOPS/Karaoke).  Unlike OOPS, though, the algorithm involves
imperfect manipulations; end result sounds great but slight distortions
(warbling, echo) cannot easily be avoided.

Very naive understanding of both DSP and C programming was harnessed for the
port; please forgive any eye bleeding.

Output chunk size will probably differ from input chunk size as enough data
needs analysis before processing; thus, this plugin will most likely never meet
the requirements for RT.

As a possible consequence of the above paragraph, using MPlayer with this
plugin and ALSA output gives sonic garbage; OSS seems far more reliable but
that output sink is getting old and crufty.

Example real-time use:

```shell
mplayer -ao oss -af ladspa=/path/to/ladspacc.so:ladspacc:1:200 music.ogg
```

Example offline use:

```shell
applyplugin in.wav out.wav /path/to/ladspacc.so ladspacc 1 200
```

First parameter:
- 0: Center Cut Mode 0: Sides
- 1: Center Cut Mode 1: Center
- 2: Center Cut Mode 2: Sides (Bass to Sides)
- 3: Center Cut Mode 3: Center (Bass to Sides)

Second parameter:
- Bass cutoff frequency in Hz; 200 is the default.

Original WinAmp plugin is there: http://moitah.net/#tog%5Fdsp%5Fcentercut

