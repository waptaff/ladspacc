Ladspa port of WinAmp CenterCut — a way to extract the “center channel” of
stereo audio files.

# Prerequisites

To compile ladspacc some basic developement packages are needed.  Apart from
the LADSPA header files, standard development packages provided with glibc
should suffice.  Here are examples to get started:

## CentOS 6, CentOS 7

```shell
sudo yum install autoconf lasdpa-devel
```

## Debian 9

```shell
sudo apt install autoconf ladspa-sdk
```

# Compilation / Installation

ladspacc uses GNU autotools, so installation should be straightforward:

```shell
autoreconf --install
./configure
make
make install
```

