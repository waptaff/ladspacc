/*******************************************************************************
*                                                                              *
* LADSPA Center Cut                                                            *
* A port of Center Cut DSP from Moitah                                         *
* This code is Copyright (C) 2009 Patrice Levesque <ladspacc.wayne@ptaff.ca>   *
*                                                                              *
* Center Cut DSP Plugin for Winamp 2/5                                         *
* Original implementation Copyright (C) 2004-2007  Moitah (moitah@yahoo.com)   *
*                                                                              *
* This program is free software; you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation; either version 2 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program; if not, write to the Free Software                  *
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    *
*                                                                              *
*******************************************************************************/

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ladspa.h"

/* Constants and datatypes */ /* {{{ */
typedef signed int         sint32;
typedef unsigned int       uint32;
typedef signed short       sint16;
typedef unsigned short     uint16;
typedef signed char        sint8;
typedef unsigned char      uint8;
typedef sint32             int32;
typedef sint16             int16;
typedef sint8              int8;
typedef enum {FALSE, TRUE} bool;

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

#define KWINDOWSIZE      8192
#define KOVERLAPCOUNT    4
#define KPOSTWINDOWPOWER 2
#define KHALFWINDOW      (KWINDOWSIZE / 2)
#define KOVERLAPSIZE     (KWINDOWSIZE / KOVERLAPCOUNT)

/* Maximum buffer sizes, should be more than enough */
#define MAX_DELAY 5

#define TWOPI       6.283185307179586476925286766559
#define INVSQRT2    0.70710678118654752440084436210485
#define NODIVBYZERO 0.000000000000001

#define LASDPA_DATA_TO_DOUBLE 0
#define DOUBLE_TO_LADSPA_DATA 1

#define MOUTPUTSAMPLECOUNT 2048
#define MOUTPUTMAXBUFFERS  32

#define SAMPLESCALEINV 32768.0
#define SAMPLESCALE    (1.0 / SAMPLESCALEINV)
#define SAMPLEMIN      -2147483648.0
#define SAMPLEMAX      2147483647.0

double       * mOutputBuffer[MOUTPUTMAXBUFFERS];
int            mOutputReadSampleOffset;
int            mOutputBufferCount;  // How many buffers are actually in use (there may be more allocated than in use)
int            mSampleRate;
bool           mOutputCenter;
bool           mBassToSides;
unsigned int   mBassFreq;
int            mOutputDiscardBlocks;
uint32         mInputSamplesNeeded;
uint32         mInputPos;
unsigned       mBitRev[KWINDOWSIZE];
double         mPreWindow[KWINDOWSIZE];
double         mPostWindow[KWINDOWSIZE];
double         mSineTab[KWINDOWSIZE];
double         mInput[KWINDOWSIZE][2];
double         mOverlapC[KOVERLAPCOUNT - 1][KOVERLAPSIZE];
double         mTempLBuffer[KWINDOWSIZE];
double         mTempRBuffer[KWINDOWSIZE];
double         mTempCBuffer[KWINDOWSIZE];

/* The port numbers for the plugin: */

#define CC_INPUT1     0
#define CC_OUTPUT1    1
#define CC_INPUT2     2
#define CC_OUTPUT2    3
#define CC_MODENUMBER 4
#define CC_BASSFREQ   5

typedef struct { /* {{{ */
	/* The structure used to hold port connection information and state
	   (actually gain controls require no further state). */
	LADSPA_Data   * m_iModeNumber;
	LADSPA_Data   * m_uiBassFreq;
	LADSPA_Data   * m_pfInputBuffer1;
	LADSPA_Data   * m_pfOutputBuffer1;
	LADSPA_Data   * m_pfInputBuffer2;
	LADSPA_Data   * m_pfOutputBuffer2;
	LADSPA_Data   * m_pfMuxedInputBuffer;
	LADSPA_Data   * m_pfMuxedOutputBuffer;
	unsigned long   m_lMuxedBufferSize;
} ConnectionState; /* }}} */

LADSPA_Descriptor * g_psCenterCutDescriptor = NULL;

/* }}} */

/* Functions imported and adapted from original CenterCut */

void OutputBufferInit() { /* {{{ */
	int i;
	for (i = 0; i < MOUTPUTMAXBUFFERS; i++) {
		mOutputBuffer[i] = 0;
	};
	mOutputBufferCount      = 0;
	mOutputReadSampleOffset = 0;
}; /* }}} */
unsigned IntegerLog2(unsigned v) { /* {{{ */
	unsigned i = 0;
	while (v > 1) {
		++i;
		v >>= 1;
	};
	return i;
}; /* }}} */
unsigned RevBits(unsigned x, unsigned bits) { /* {{{ */
	unsigned y = 0;
	while (bits--) {
		y = (y + y) + (x & 1);
		x >>= 1;
	};
	return y;
}; /* }}} */
void VDCreateBitRevTable(unsigned *dst, int n) { /* {{{ */
	int i;
	unsigned bits = IntegerLog2(n);
	for (i = 0; i < n; ++i) {
		dst[i] = RevBits(i, bits);
	};
}; /* }}} */
void VDCreateHalfSineTable(double *dst, int n) { /* {{{ */
	int i;
	double TWOPI_over_n = TWOPI / n;
	for (i = 0; i < n; ++i) {
		dst[i] = sin(TWOPI_over_n * i);
	};
}; /* }}} */
void VDCreateRaisedCosineWindow(double *dst, int n, double power) { /* {{{ */
	int i;
	double TWOPI_over_n = TWOPI / n;
	double scalefac = 1.0 / n;

	for (i = 0; i < n; ++i) {
		dst[i] = scalefac * pow(0.5 * (1.0 - cos(TWOPI_over_n * (i + 0.5))), power);
	};
}; /* }}} */
void CreatePostWindow(double *dst, int windowSize, int power) { /* {{{ */
	int i;
	double powerIntegrals[8] = {
		  1.0,
		  1.0 / 2.0,
		  3.0 / 8.0,
		  5.0 / 16.0,
		 35.0 / 128.0,
		 63.0 / 256.0,
		231.0 / 1024.0,
		429.0 / 2048.0
	};
	double scalefac = (double)windowSize * (powerIntegrals[1] / powerIntegrals[power + 1]);

	VDCreateRaisedCosineWindow(dst, windowSize, (double)power);

	for (i = 0; i < windowSize; ++i) {
		dst[i] *= scalefac;
	};
}; /* }}} */
void VDComputeFHT(double *A, int nPoints, const double *sinTab) { /* {{{ */
	int    i, j, n, n2, n4, theta, theta_inc;
	double x0, x1, x2, x3, y0, y1, y2, y3;
	double cosval, sinval;
	double alpha, alpha1, alpha2;
	double beta, beta1, beta2;

	// FHT - stage 1 and 2 (2 and 4 points)

	for (i = 0; i < nPoints; i += 4) {
		x0 = A[i];
		x1 = A[i + 1];
		x2 = A[i + 2];
		x3 = A[i + 3];

		y0 = x0 + x1;
		y1 = x0 - x1;
		y2 = x2 + x3;
		y3 = x2 - x3;

		A[i + 0] = y0 + y2;
		A[i + 2] = y0 - y2;

		A[i + 1] = y1 + y3;
		A[i + 3] = y1 - y3;
	};

	// FHT - stage 3 (8 points)

	for (i = 0; i < nPoints; i+= 8) {
		alpha    = A[i + 0];
		beta     = A[i + 4];

		A[i + 0] = alpha + beta;
		A[i + 4] = alpha - beta;

		alpha    = A[i + 2];
		beta     = A[i + 6];

		A[i + 2] = alpha + beta;
		A[i + 6] = alpha - beta;

		alpha    = A[i + 1];

		beta1    = INVSQRT2 * (A[i + 5] + A[i + 7]);
		beta2    = INVSQRT2 * (A[i + 5] - A[i + 7]);

		A[i + 1] = alpha + beta1;
		A[i + 5] = alpha - beta1;

		alpha    = A[i + 3];

		A[i + 3] = alpha + beta2;
		A[i + 7] = alpha - beta2;
	};

	n = 16;
	n2 = 8;
	theta_inc = nPoints >> 4;

	while (n <= nPoints) {
		for (i = 0; i < nPoints; i += n) {
			theta = theta_inc;
			n4 = n2>>1;

			alpha          = A[i];
			beta           = A[i + n2];

			A[i]           = alpha + beta;
			A[i + n2]      = alpha - beta;

			alpha          = A[i + n4];
			beta           = A[i + n2 + n4];

			A[i + n4]      = alpha + beta;
			A[i + n2 + n4] = alpha - beta;

			for (j = 1; j < n4; j++) {
				sinval         = sinTab[theta];
				cosval         = sinTab[theta + (nPoints>>2)];

				alpha1         = A[i + j];
				alpha2         = A[i - j + n2];
				beta1          = A[i + j + n2] * cosval + A[i - j + n] * sinval;
				beta2          = A[i + j + n2] * sinval - A[i - j + n] * cosval;

				theta         += theta_inc;

				A[i + j]       = alpha1 + beta1;
				A[i + j + n2]  = alpha1 - beta1;
				A[i - j + n2]  = alpha2 + beta2;
				A[i - j + n]   = alpha2 - beta2;
			};
		};

		n          *= 2;
		n2         *= 2;
		theta_inc >>= 1;
	};
}; /* }}} */
void ConvertSamples(int type, LADSPA_Data *sampB, double *sampD, int sampleCount) { /* {{{ */
	/* Original function is mostly useless as we get floats and need doubles. */
	/* Rewritten */
	int lSampleIndex;
	if (type == LASDPA_DATA_TO_DOUBLE) {
		for (lSampleIndex = 0; lSampleIndex < sampleCount; lSampleIndex++) {
			*(sampD) = (double)(*(sampB));
			sampD++;
			sampB++;
		};
	}
	else {
		for (lSampleIndex = 0; lSampleIndex < sampleCount; lSampleIndex++) {
			*(sampB) = (LADSPA_Data)(*(sampD));
			sampD++;
			sampB++;
		};
	};
	return;
}; /* }}} */
bool OutputBufferBeginWrite() { /* {{{ */
	int i;
	int buffersize;
	if (mOutputBufferCount == MOUTPUTMAXBUFFERS) {
		return FALSE;
	};

	i = mOutputBufferCount;
	if (!mOutputBuffer[i]) {
		buffersize = sizeof(double) * MOUTPUTSAMPLECOUNT * 2;
		mOutputBuffer[i] = (double *) calloc(MOUTPUTSAMPLECOUNT * 2, sizeof(double));
		if (!mOutputBuffer[i]) {
			return FALSE;
		};
	};
	mOutputBufferCount++;
	return TRUE;
}; /* }}} */
bool CenterCut_Run() { /* {{{ */
	unsigned i, j, k;
	double w;
	double lR, lI, rR, rI, sumR, sumI, diffR, diffI, sumSq, diffSq, alpha, cR, cI;
	double c, l, r;
	double * outBuffer;
	int currentBlockIndex, nextBlockIndex, blockOffset;
	unsigned int freqBelowToSides = (unsigned int)(((double)mBassFreq / ((double)mSampleRate / (double)KWINDOWSIZE)) + 0.5);

	// copy to temporary buffer and FHT

	for (i = 0; i < KWINDOWSIZE; ++i) {
		j = mBitRev[i];
		k = (j + mInputPos) & (KWINDOWSIZE - 1);
		w = mPreWindow[i];
		mTempLBuffer[i] = mInput[k][0] * w;
		mTempRBuffer[i] = mInput[k][1] * w;
	};

	VDComputeFHT(mTempLBuffer, KWINDOWSIZE, mSineTab);
	VDComputeFHT(mTempRBuffer, KWINDOWSIZE, mSineTab);

	// perform stereo separation

	mTempCBuffer[0] = 0;
	mTempCBuffer[1] = 0;
	for (i = 1; i < KHALFWINDOW; i++) {
		lR     = mTempLBuffer[i] + mTempLBuffer[KWINDOWSIZE - i];
		lI     = mTempLBuffer[i] - mTempLBuffer[KWINDOWSIZE - i];
		rR     = mTempRBuffer[i] + mTempRBuffer[KWINDOWSIZE - i];
		rI     = mTempRBuffer[i] - mTempRBuffer[KWINDOWSIZE - i];
		sumR   = lR + rR;
		sumI   = lI + rI;
		diffR  = lR - rR;
		diffI  = lI - rI;
		sumSq  = sumR * sumR + sumI * sumI;
		diffSq = diffR * diffR + diffI * diffI;
		alpha  = 0.0;
		if (sumSq > NODIVBYZERO) {
			alpha = 0.5 - sqrt(diffSq / sumSq) * 0.5;
		};
		cR = sumR * alpha;
		cI = sumI * alpha;
		if (mBassToSides && (i < freqBelowToSides)) {
			cR = cI = 0.0;
		};
		mTempCBuffer[mBitRev[i              ]] = cR + cI;
		mTempCBuffer[mBitRev[KWINDOWSIZE - i]] = cR - cI;
	};

	// reconstitute left/right/center channels
	VDComputeFHT(mTempCBuffer, KWINDOWSIZE, mSineTab);

	// apply post-window
	for (i = 0; i < KWINDOWSIZE; i++) {
		mTempCBuffer[i] *= mPostWindow[i];
	};

	// writeout
	if (mOutputDiscardBlocks > 0) {
		mOutputDiscardBlocks--;
	}
	else {
		if (!OutputBufferBeginWrite()) {
			return FALSE;
		};
		outBuffer = mOutputBuffer[mOutputBufferCount - 1];
		if (!outBuffer) {
			return FALSE;
		};

		for (i = 0; i < KOVERLAPSIZE; ++i) {
			c = mOverlapC[0][i] + mTempCBuffer[i];
			l = mInput[mInputPos + i][0] - c;
			r = mInput[mInputPos + i][1] - c;

			if (mOutputCenter) {
				outBuffer[0] = c;
				outBuffer[1] = c;
			}
			else {
				outBuffer[0] = l;
				outBuffer[1] = r;
			};
			outBuffer += 2;

			// overlapping

			currentBlockIndex = 0;
			nextBlockIndex    = 1;
			blockOffset       = KOVERLAPSIZE;
			while (nextBlockIndex < (KOVERLAPCOUNT - 1)) {
				mOverlapC[currentBlockIndex][i] = mOverlapC[nextBlockIndex][i] + mTempCBuffer[blockOffset + i];
				currentBlockIndex++;
				nextBlockIndex++;
				blockOffset += KOVERLAPSIZE;
			};
			mOverlapC[currentBlockIndex][i] = mTempCBuffer[blockOffset + i];
		};
	};
	mInputSamplesNeeded = KOVERLAPSIZE;
	return TRUE;
}; /* }}} */
void OutputBufferReadComplete() { /* {{{ */
	int i;
	double * moveToEnd;
	mOutputBufferCount--;
	mOutputReadSampleOffset = 0;
	if (mOutputBufferCount > 0) {
		moveToEnd = mOutputBuffer[0];

		// Shift the buffers so that the current one for reading is at index 0
		for (i = 1; i < MOUTPUTMAXBUFFERS; i++) {
			mOutputBuffer[i - 1] = mOutputBuffer[i];
		};
		mOutputBuffer[MOUTPUTMAXBUFFERS - 1] = 0;

		// Move the previous first buffer to the end (first null pointer)
		for (i = 0; i < MOUTPUTMAXBUFFERS; i++) {
			if (!mOutputBuffer[i]) {
				mOutputBuffer[i] = moveToEnd;
				break;
			};
		};
	};
}; /* }}} */
int CenterCutProcessSamples(LADSPA_Data *inSamples, int inSampleCount, LADSPA_Data *outSamples, bool outputCenter, bool bassToSides, unsigned int iBassFreq) { /* {{{ */
	int outSampleCount, maxOutSampleCount, copyCount;
	double * sampD;

	mOutputCenter     = outputCenter;
	mBassToSides      = bassToSides;
	mBassFreq         = iBassFreq;
	outSampleCount    = 0;
	maxOutSampleCount = inSampleCount;

	while (inSampleCount > 0) {
		copyCount = min((int)mInputSamplesNeeded, inSampleCount);

		ConvertSamples(LASDPA_DATA_TO_DOUBLE, inSamples, &mInput[mInputPos][0], copyCount * 2);

		inSamples           += copyCount * 2;
		inSampleCount       -= copyCount;
		mInputPos           = (mInputPos + (copyCount)) & (KWINDOWSIZE - 1);
		mInputSamplesNeeded -= copyCount;

		if (mInputSamplesNeeded == 0) {
			CenterCut_Run();
		};
	};

	while ((mOutputBufferCount > 0) && (outSampleCount < maxOutSampleCount)) {
		sampD = mOutputBuffer[0];
		if (!sampD) {
			return -1;
		};

		copyCount = min(MOUTPUTSAMPLECOUNT - mOutputReadSampleOffset, maxOutSampleCount - outSampleCount);

		ConvertSamples(DOUBLE_TO_LADSPA_DATA, outSamples, sampD + (mOutputReadSampleOffset), copyCount * 2);

		outSamples              += copyCount;
		outSampleCount          += copyCount * 2;
		mOutputReadSampleOffset += copyCount;
		if (mOutputReadSampleOffset == MOUTPUTSAMPLECOUNT) {
			OutputBufferReadComplete();
		};
	};
	return outSampleCount;
}; /* }}} */

/* LADSPA implementation */

LADSPA_Handle instantiateCenterCut(const LADSPA_Descriptor * Descriptor, unsigned long SampleRate) { /* {{{ */
	unsigned long     lMinimumBufferSize;
	ConnectionState * ConnectionStatePtr;

	/* Buffer size is a power of two bigger than max delay time. */
	lMinimumBufferSize                     = (unsigned long)((LADSPA_Data)SampleRate * MAX_DELAY);
	mSampleRate                            = (int)SampleRate;
	ConnectionStatePtr                     = (ConnectionState *)calloc(1, sizeof(ConnectionState));
	ConnectionStatePtr->m_lMuxedBufferSize = 1;
	while (ConnectionStatePtr->m_lMuxedBufferSize < lMinimumBufferSize) {
		ConnectionStatePtr->m_lMuxedBufferSize <<= 1;
	};
	ConnectionStatePtr->m_pfMuxedInputBuffer  = (LADSPA_Data *)calloc(ConnectionStatePtr->m_lMuxedBufferSize * 2, sizeof(LADSPA_Data));
	ConnectionStatePtr->m_pfMuxedOutputBuffer = (LADSPA_Data *)calloc(ConnectionStatePtr->m_lMuxedBufferSize * 2, sizeof(LADSPA_Data));
	return ConnectionStatePtr;
}; /* }}} */
void connectPortToCenterCut(LADSPA_Handle Instance, unsigned long Port, LADSPA_Data * DataLocation) { /* {{{ */
	/* Connect a port to a data location. */

	ConnectionState * psCenterCut;

	psCenterCut = (ConnectionState *)Instance;
	switch (Port) {
		case CC_MODENUMBER:
			psCenterCut->m_iModeNumber     = DataLocation;
			break;
		case CC_BASSFREQ:
			psCenterCut->m_uiBassFreq      = DataLocation;
			break;
		case CC_INPUT1:
			psCenterCut->m_pfInputBuffer1  = DataLocation;
			break;
		case CC_OUTPUT1:
			psCenterCut->m_pfOutputBuffer1 = DataLocation;
			break;
		case CC_INPUT2:
			psCenterCut->m_pfInputBuffer2  = DataLocation;
			break;
		case CC_OUTPUT2:
			psCenterCut->m_pfOutputBuffer2 = DataLocation;
			break;
	};
}; /* }}} */
void runCenterCut(LADSPA_Handle Instance, unsigned long SampleCount) { /* {{{ */
	/* The actual main DSP */
	LADSPA_Data     * pfInput1;
	LADSPA_Data     * pfInput2;
	LADSPA_Data     * pfOutput1;
	LADSPA_Data     * pfOutput2;
	LADSPA_Data     * pfMuxedInput;
	LADSPA_Data     * pfMuxedOutput;
	LADSPA_Data     * pfMuxedReset;
	ConnectionState * psCenterCut;
	unsigned long     lSampleIndex;
	unsigned int      iModeNumber;
	unsigned int      iBassFreq;
	unsigned int      numberOfReturnedSamples;

	psCenterCut = (ConnectionState *)Instance;

	iModeNumber = *(psCenterCut->m_iModeNumber);
	iBassFreq   = *(psCenterCut->m_uiBassFreq);
	if (iBassFreq == 0) {
		iBassFreq = 200;
	};

	pfInput1      = psCenterCut->m_pfInputBuffer1;
	pfInput2      = psCenterCut->m_pfInputBuffer2;
	pfOutput1     = psCenterCut->m_pfOutputBuffer1;
	pfOutput2     = psCenterCut->m_pfOutputBuffer2;
	pfMuxedInput  = psCenterCut->m_pfMuxedInputBuffer;
	pfMuxedOutput = psCenterCut->m_pfMuxedOutputBuffer;
	// mux
	pfMuxedReset = pfMuxedInput;
	for (lSampleIndex = 0; lSampleIndex < SampleCount; lSampleIndex++) {
		*(pfMuxedInput) = *(pfInput1);
		pfMuxedInput++;
		pfInput1++;
		*(pfMuxedInput) = *(pfInput2);
		pfMuxedInput++;
		pfInput2++;
	};
	pfMuxedInput = pfMuxedReset;
	numberOfReturnedSamples = CenterCutProcessSamples(pfMuxedInput, SampleCount, pfMuxedOutput, iModeNumber & 1, (iModeNumber & 2) >> 1, iBassFreq);
	for (lSampleIndex = numberOfReturnedSamples; lSampleIndex < (SampleCount * 2); lSampleIndex++) {
		*(pfOutput1) = (LADSPA_Data)0.0;
		*(pfOutput2) = (LADSPA_Data)0.0;
	};
	for (lSampleIndex = 0; lSampleIndex < min(SampleCount * 2, numberOfReturnedSamples); lSampleIndex++) {
		*(pfOutput1) = *(pfMuxedOutput);
		pfOutput1++;
		pfMuxedOutput++;
		lSampleIndex++;
		*(pfOutput2) = *(pfMuxedOutput);
		pfOutput2++;
		pfMuxedOutput++;
	};
}; /* }}} */
void cleanupCenterCut(LADSPA_Handle Instance) { /* {{{ */
	/* Cleanup stuff */
	free(Instance);
}; /* }}} */
void _init() { /* {{{ */
	/* _init() is called automatically when the plugin library is first loaded. */
	unsigned i;
	char ** pcPortNames;
	LADSPA_PortDescriptor * piPortDescriptors;
	LADSPA_PortRangeHint * psPortRangeHints;

	g_psCenterCutDescriptor = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

	if (g_psCenterCutDescriptor) {
		g_psCenterCutDescriptor->UniqueID              = 4341;
		g_psCenterCutDescriptor->Label                 = strdup("ladspacc");
		g_psCenterCutDescriptor->Properties            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
		g_psCenterCutDescriptor->Name                  = strdup("LADSPA Center Cut");
		g_psCenterCutDescriptor->Maker                 = strdup("Patrice Levesque (adapted from Moitah)");
		g_psCenterCutDescriptor->Copyright             = strdup("GPL");
		g_psCenterCutDescriptor->PortCount             = 6;
		piPortDescriptors                              = (LADSPA_PortDescriptor *)calloc(6, sizeof(LADSPA_PortDescriptor));
		g_psCenterCutDescriptor->PortDescriptors       = (const LADSPA_PortDescriptor *)piPortDescriptors;
		piPortDescriptors[CC_INPUT1]                   = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
		piPortDescriptors[CC_OUTPUT1]                  = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
		piPortDescriptors[CC_INPUT2]                   = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
		piPortDescriptors[CC_OUTPUT2]                  = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
		piPortDescriptors[CC_MODENUMBER]               = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
		piPortDescriptors[CC_BASSFREQ]                 = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
		pcPortNames                                    = (char **)calloc(6, sizeof(char *));
		g_psCenterCutDescriptor->PortNames             = (const char **)pcPortNames;
		pcPortNames[CC_INPUT1]                         = strdup("Input (Left)");
		pcPortNames[CC_OUTPUT1]                        = strdup("Output (Left)");
		pcPortNames[CC_INPUT2]                         = strdup("Input (Right)");
		pcPortNames[CC_OUTPUT2]                        = strdup("Output (Right)");
		pcPortNames[CC_MODENUMBER]                     = strdup("Mode; 0:Sides, 1:SidesBTS, 2:Center, 3:CenterBTS");
		pcPortNames[CC_BASSFREQ]                       = strdup("Upperbound bass frequency for modes {1,3}; unset or 0 gives 200");
		psPortRangeHints                               = ((LADSPA_PortRangeHint *) calloc(6, sizeof(LADSPA_PortRangeHint)));
		g_psCenterCutDescriptor->PortRangeHints        = (const LADSPA_PortRangeHint *)psPortRangeHints;
		psPortRangeHints[CC_INPUT1].HintDescriptor     = 0;
		psPortRangeHints[CC_OUTPUT1].HintDescriptor    = 0;
		psPortRangeHints[CC_INPUT2].HintDescriptor     = 0;
		psPortRangeHints[CC_OUTPUT2].HintDescriptor    = 0;
		psPortRangeHints[CC_MODENUMBER].HintDescriptor = (LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_INTEGER | LADSPA_HINT_DEFAULT_0);
		psPortRangeHints[CC_MODENUMBER].LowerBound     = 0;
		psPortRangeHints[CC_MODENUMBER].UpperBound     = 3;
		psPortRangeHints[CC_BASSFREQ].HintDescriptor   = (LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_INTEGER | LADSPA_HINT_DEFAULT_0);
		psPortRangeHints[CC_BASSFREQ].LowerBound       = 0;
		psPortRangeHints[CC_BASSFREQ].UpperBound       = 22050;
		g_psCenterCutDescriptor->instantiate           = instantiateCenterCut;
		g_psCenterCutDescriptor->connect_port          = connectPortToCenterCut;
		g_psCenterCutDescriptor->activate              = NULL;
		g_psCenterCutDescriptor->run                   = runCenterCut;
		g_psCenterCutDescriptor->run_adding            = NULL;
		g_psCenterCutDescriptor->set_run_adding_gain   = NULL;
		g_psCenterCutDescriptor->deactivate            = NULL;
		g_psCenterCutDescriptor->cleanup               = cleanupCenterCut;
	};
	OutputBufferInit();
	VDCreateBitRevTable(mBitRev, KWINDOWSIZE);
	VDCreateHalfSineTable(mSineTab, KWINDOWSIZE);
	mInputSamplesNeeded  = KOVERLAPSIZE;
	mInputPos            = 0;
	mOutputDiscardBlocks = KOVERLAPCOUNT - 1;

	memset(mInput,    0, sizeof mInput);
	memset(mOverlapC, 0, sizeof mOverlapC);
	double *tmp = (double *) calloc(KWINDOWSIZE, sizeof(double));
	VDCreateRaisedCosineWindow(tmp, KWINDOWSIZE, 1.0);
	for (i = 0; i < KWINDOWSIZE; ++i) {
		// The correct Hartley<->FFT conversion is:
		//
		//	Fr(i) = 0.5(Hr(i) + Hi(i))
		//	Fi(i) = 0.5(Hr(i) - Hi(i))
		//
		// We omit the 0.5 in both the forward and reverse directions,
		// so we have a 0.25 to put here.

		mPreWindow[i] = tmp[mBitRev[i]] * 0.5 * (2.0 / (double)KOVERLAPCOUNT);
	};
	free(tmp);
	CreatePostWindow(mPostWindow, KWINDOWSIZE, KPOSTWINDOWPOWER);

}; /* }}} */
void deleteDescriptor(LADSPA_Descriptor * psDescriptor) { /* {{{ */
	unsigned long lIndex;
	if (psDescriptor) {
		free((char *)psDescriptor->Label);
		free((char *)psDescriptor->Name);
		free((char *)psDescriptor->Maker);
		free((char *)psDescriptor->Copyright);
		free((LADSPA_PortDescriptor *)psDescriptor->PortDescriptors);
		for (lIndex = 0; lIndex < psDescriptor->PortCount; lIndex++) {
			free((char *)(psDescriptor->PortNames[lIndex]));
		};
		free((char **)psDescriptor->PortNames);
		free((LADSPA_PortRangeHint *)psDescriptor->PortRangeHints);
		free(psDescriptor);
	};
}; /* }}} */
void _fini() { /* {{{ */
	/* _fini() is called automatically when the library is unloaded. */
	int i;
	deleteDescriptor(g_psCenterCutDescriptor);
	for (i = 0; i < MOUTPUTMAXBUFFERS; i++) {
		if (mOutputBuffer[i]) {
			free(mOutputBuffer[i]);
			mOutputBuffer[i] = 0;
		};
	};
}; /* }}} */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long Index) { /* {{{ */
	/* Return a descriptor of the requested plugin type. There are two
	   plugin types available in this library (mono and stereo). */
	switch (Index) {
		case 0:
			return g_psCenterCutDescriptor;
		default:
			return NULL;
	};
}; /* }}} */
